import React from 'react';

const Footer = () => {
    return(
        <footer className="page-footer">
          <div className="container">
            <h5>Pharmapi</h5>
          </div>
          <div className="footer-copyright">
            <div className="container">
            © 2021 Copyright Pharmapi Build With React
            <a className="grey-text text-lighten-4 right" href="#!">Mention Légal</a>
            </div>
          </div>
        </footer>
    )
}

export default Footer;