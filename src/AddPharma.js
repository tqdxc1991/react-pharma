import React, {Component} from 'react';


class AddPharma extends Component {
    state = {nom:"",quartier:"",ville:"",garde:""};

    handleChange = (e) => {
        this.setState({
            [e.target.id]:e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("added successfully",this.state);
        this.props.addPharma(this.state);
        this.setState({nom:"",quartier:"",ville:"",garde:""});
    }

    render(){
        return(
            <div>
                <form onSubmit={this.handleSubmit}>
                <label>nom: </label>
                <input type="text" onChange={this.handleChange} id="nom" value={this.state.nom}></input>
                <label>quartier: </label>
                <input type="text" onChange={this.handleChange} id="quartier" value={this.state.quartier}></input>
                <label>ville: </label>
                <input type="text" onChange={this.handleChange} id="ville" value={this.state.ville}></input>
                <label>garde: </label>
                <input type="text" onChange={this.handleChange} id="garde" value={this.state.garde}></input>
                <button id="addButton">Add a pharmacy</button>
                </form>
            </div>
        )
    }
}

export default AddPharma;