import React from 'react';
import logo from './logo.jpg';

const Navbar = () => {
           
    return(
          <nav>
            <div className="nav-wrapper container">
            <a href="/" className="brand-logo center"><img src={logo} alt="logo" /></a>
            
                <a href="/">HOME</a>
            </div>
        </nav>
    )
}

export default Navbar;
