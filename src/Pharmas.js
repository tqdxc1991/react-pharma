import React from 'react';
import EditPharma from './EditPharma';

const Pharmas = ({pharmas, gardes, showGarde, deletePharma, editPharma}) => {

    const [dislayForm, setdislayForm] = React.useState(false)

   // const showForm = () => {
   //     setdislayForm(!dislayForm);

        /*
        if (displayForm) {
                    setdislayForm(false);
        }
        else {
                    setdislayForm(true);
        }
        
        */
  //  }

    const pharmaList = showGarde ?  (
        gardes.map(garde => {
            return(
                <div className="collection-item" key={garde.id}>

                      <span> {garde.nom} - {garde.quartier} - {garde.ville} - {garde.garde} 
                      </span>
                      
                </div>
            )
        })
    ):(
        pharmas.map(pharma => {
            return(
                <div className="collection-item" key={pharma.id}>
                     

                      <span >{pharma.nom} - {pharma.quartier} - {pharma.ville} - {pharma.garde} 
                     </span>
                     
                     <div className="right">
                     <button onClick={()=>{deletePharma(pharma.id)}}> DELETE</button>
                      <button onClick={(e)=>{setdislayForm(!dislayForm);
                      console.log(pharma.id,e);
                      e.target.scrollIntoView();
                      }}   > EDIT</button>
                     </div>
                      { dislayForm ? <EditPharma pharma={pharma} editPharma={editPharma} /> : null }
                     
                </div>
            )
        })
    ) ;

    return (
        <div className="collection center">
            {pharmaList}
        </div>
    )
}

export default Pharmas;