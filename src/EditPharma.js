import React, {Component} from 'react';


class EditPharma extends Component {
    state = {id:this.props.pharma.id,
             nom:this.props.pharma.nom,
             quartier:this.props.pharma.quartier,
             ville:this.props.pharma.ville,
             garde:this.props.pharma.garde};

    handleChange = (e) => {
        this.setState({
            [e.target.id]:e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        //pass the state to parent component
        this.props.editPharma(this.state);
        
    }

    render(){
        return(
            <div>
                <form onSubmit={this.handleSubmit}>
                <label>nom: </label>
                <input type="text" onChange={this.handleChange} id="nom" value={this.state.nom}></input>
                <label>quartier: </label>
                <input type="text" onChange={this.handleChange} id="quartier" value={this.state.quartier}></input>
                <label>ville: </label>
                <input type="text" onChange={this.handleChange} id="ville" value={this.state.ville}></input>
                <label>garde: </label>
                <input type="text" onChange={this.handleChange} id="garde" value={this.state.garde}></input>
                <button>OK</button>
                </form>
            </div>
        )
    }
}

export default EditPharma;