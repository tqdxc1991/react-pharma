import React, {Component} from 'react';
import Pharmas from './Pharmas';
import AddPharma from './AddPharma';
import axios from 'axios';
import Navbar from './Navbar';
import Footer from './Footer';


class App extends Component {
  
  state = {
    pharmas:[],
    gardes:[],
    showGarde:false
    
  }



  togglePharma = ()=>{ 
    this.setState({showGarde:true})
   }

   backPharma = ()=>{ 
    
    this.setState({showGarde:false})
   }


componentDidMount(){
      //list de pharmacie de garde
      axios.get('https://127.0.0.1:8000/pharma-garde') 
      .then(res => {
        console.log (res);
        this.setState({gardes:res.data})
         })  
    
      //list de pharmacie   
      axios.get('https://127.0.0.1:8000/pharma') 
      .then(res => {
        console.log (res);
        this.setState({pharmas:res.data})
         })  
    
      
  }

  //functional component no state
  deletePharma = (id) => {
    //choose the pharmas which has not the same id and update the state only front-end
    // const pharmas = this.state.pharmas.filter(
    //  pharma => {return pharma.id !== id}
   // )
   // this.setState({pharmas});

        //delete a pharma by api
       axios.delete('https://127.0.0.1:8000/pharma/'+id)
          .then(res =>
           {
             console.log("delete a pharma",res);
                       //show the new list
            axios.get('https://127.0.0.1:8000/pharma') 
           .then(res => {
            console.log ("show the new list",res);
            this.setState({pharmas:res.data})
           }) 
          } 
          )
  }

  editPharma = (pharma) =>{

    console.log("APP edit pharma",pharma)

  
        const id =  pharma.id;
        
        //edit a pharma by api
        axios.put('https://127.0.0.1:8000/pharma/'+id, pharma)
         .then(res =>
          {
            console.log("edit a pharma",res);
                      //show the new list
           axios.get('https://127.0.0.1:8000/pharma') 
          .then(res => {
           console.log ("show the new list",res);
           this.setState({pharmas:res.data})
        }) 
         } 
         )
}
  

  //functional component 
  addPharma = (pharma) => {
    //refresh date in the front-end without api
    // pharma.id = Math.random();
    // let pharmas = [...this.state.pharmas,pharma ];
    // this.setState({ pharmas }); 

    //add a pharma by api
    axios({
      method: 'post',
      url: 'https://127.0.0.1:8000/pharma',
      data: pharma })
      .then(res =>
       {
          console.log("add a pharma",res);
          //show the new list
          axios.get('https://127.0.0.1:8000/pharma') 
          .then(res => {
            console.log ("show the new list",res);
            this.setState({pharmas:res.data})
           }) 
        }
      )
      
      

  }

  render(){
    return(
      <div className="App container">
      <Navbar showGarde={this.state.showGarde}/>
      <h1 className="center" id="title">Pharmapi</h1>
      <button onClick={this.togglePharma}>Pharmacie de Garde</button>  
      <button onClick={this.backPharma}>Pharmacie list</button>
      <Pharmas  pharmas = {this.state.pharmas}  gardes = {this.state.gardes} showGarde={this.state.showGarde} deletePharma= {this.deletePharma} editPharma= {this.editPharma}/>

      <AddPharma addPharma= {this.addPharma}/>
      <Footer />
      </div>
    

    );
  }

}  

export default App;